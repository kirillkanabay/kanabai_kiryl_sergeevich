﻿using Chat.Models;
using Chat.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Chat.Controllers
{
    public class AuthController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(User user)
        {
            if (ModelState.IsValid)
            {
                HttpContext.Session.Set("user", user);
                return RedirectToAction("Index", "Chat");
            }

            return View(user);
        }
    }
}
