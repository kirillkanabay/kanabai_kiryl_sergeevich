﻿using Chat.Extensions;
using Chat.Models;
using Chat.Services;
using Chat.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Chat.Controllers
{
    public class ChatController : Controller
    {
        private readonly IChatService _chatService;
        
        public ChatController(IChatService chatService)
        {
            _chatService = chatService;
        }

        public IActionResult Index(string searchQuery)
        {
            var user = HttpContext.Session.Get<User>("user");

            if (user == null)
            {
                return RedirectToAction("Index", "Auth");
            }

            IList<Message> messages = null; 

            if(string.IsNullOrWhiteSpace(searchQuery))
            {
                messages = _chatService.ListAll();
            }
            else
            {
                messages = _chatService.Find(searchQuery);
            }

            return View(new ChatViewModel() 
            { 
                User = user, 
                Messages = messages, 
                Message = new Message(), 
                IsTimeout = user.IsTimeout, 
                TimeoutSeconds = user.IsTimeout ? 30 - Convert.ToInt32(user.LastMessagePeriod) : 0});
        }

        [HttpPost]
        public IActionResult SendMessage(ChatViewModel model)
        {
            var user = HttpContext.Session.Get<User>("user");
            user.LastMessageTime = System.DateTime.Now;
            HttpContext.Session.Set<User>("user", user);

            _chatService.Send(model.Message);
                       

            return RedirectToAction("Index");
        }
    }
}
