﻿using Chat.Models;
using System.Collections.Generic;

namespace Chat.ViewModels
{
    public class ChatViewModel
    {
        public User User { get; set; } 

        public IList<Message> Messages { get; set; }

        public Message Message { get; set; }

        public bool IsTimeout { get; set; }

        public int TimeoutSeconds { get; set; }
    }
}
