﻿using Chat.Models;
using System.Collections.Generic;

namespace Chat.Services
{
    public interface IChatService
    {
        void Send(Message message);
        IList<Message> ListAll();
        IList<Message> Find(string query);
    }
}
