﻿using Chat.Models;
using System.Collections.Generic;
using System.Linq;

namespace Chat.Services
{
    public class ChatService : IChatService
    {
        private readonly IList<Message> _messages;

        public ChatService()
        {
            _messages = new List<Message>();
        }

        public IList<Message> Find(string querySelector)
        {
            querySelector = querySelector.ToLower();
            return _messages.Where(i => i.Content.ToLower().Contains(querySelector)).ToList();
        }

        public IList<Message> ListAll()
        {
            return _messages;
        }

        public void Send(Message message)
        {
            _messages.Add(message);
        }
    }
}
