﻿using Chat.Enums;
using System.ComponentModel.DataAnnotations;

namespace Chat.Models
{
    public class Message
    {
        public User User { get; set; }
        public string Content { get; set; }
        public MessageType Type { get; set; }

        public Message()
        {
            User = new User();
        }
    }
}
