﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Chat.Models
{
    public class User
    {
        [RegularExpression("^[a-zA-Z0-9]{3,10}$",
            ErrorMessage = "Неверное имя")]
        [MinLength(3)]
        [MaxLength(10)]
        public string Username { get; set; }
        public DateTime LastMessageTime { get; set; } 

        public double LastMessagePeriod => (DateTime.Now.Subtract(LastMessageTime)).TotalSeconds;

        public bool IsTimeout => LastMessagePeriod < 30;

        public User()
        {
            LastMessageTime = DateTime.MinValue;
        }
    }
}
